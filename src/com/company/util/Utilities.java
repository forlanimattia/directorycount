package com.company.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Utilities {
    public static Scanner sc = new Scanner(System.in);

    public static boolean isValidPath(String inp){
        try {
            return Paths.get(inp).toFile().exists();
        }catch (InvalidPathException e){
            return false;
        }
    }

    public static Stream<String> checkPath(Stream<String> a, boolean input){

        return Optional.ofNullable(a).map(d -> d.
                map(s-> Path.of(s)).map(dd -> dd.toFile()).flatMap(n -> {
                return n.isDirectory()?
                        Stream.concat(Stream.of("folder"), checkPath(convert(n.toString(),input), input)) :
                        Stream.of(n.getName());
        })).orElse(null);
    }

    public static Stream<String> convert(String a,boolean hidden){
        return Optional.ofNullable(a).
                map(e -> {
                    try {
                        Stream<Path> paths = Files.list(Path.of(e));
                        return hidden ? paths : paths.filter(s->!s.toFile().isHidden());
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                        return null;
                    }
                }).
                map(s -> s.map(d -> d.toString())).orElse(null);
    }

    public static boolean checkHidden(){
        System.out.println("Vuoi conteggiare anche i file e folder nascosti? Premi 1 per accettare, qualsiasi altro tasto per rifiutare");
        String b = sc.next();
        return b.equals("1") ? true :
                false;

    }

    public static Map<String,Long> countFolderAndFiles(Stream<String> a){
        if (a!= null){
            return a.map(o -> {
                return o.contains(".") ?
                        o.substring(o.lastIndexOf(".") + 1) :
                        !o.equalsIgnoreCase("folder") ?
                                "File with no extension":
                                o;
            }).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        }else return null;
    }

public static void count(Map<String, Long> a){
        a.forEach((e,b) -> System.out.println("-> " + e + " : " + b));
}

}
